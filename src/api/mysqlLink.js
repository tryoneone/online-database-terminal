// 登录接口
import {axios} from '../utils/request'

// export function login(parameter) {
//   return axios({
//     url: userApi.test,
//     method: 'get',
//     params: parameter
//   })
// }

export function mysqlLink(params) {
  return axios({
    url: '/mysqlLink',
    method: 'post',
    params: params
  })
}
