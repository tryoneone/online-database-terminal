import {axios} from '../utils/request'

export function getTableData(params) {
  return axios({
    url: '/getTable',
    method: 'post',
    params: params
  })
}

export function addAndChangeCols(params) {
  return axios({
    url: '/addAndChangeCol',
    method: 'post',
    params: params
  })
}

export function deleteCols(params) {
  return axios({
    url: '/deleteCol',
    method: 'post',
    params: params
  })
}
